package todolist.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import todolist.model.Task;
@Repository
public interface CRUDRepository extends JpaRepository<Task, Long> {

}
