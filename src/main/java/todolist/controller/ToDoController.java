package todolist.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import todolist.jpa.CRUDRepository;
import todolist.model.Task;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class ToDoController implements Controller {
	@Autowired
	private CRUDRepository jpa;

	@PostMapping("/save/task")
	@Override
	public String savetask(@RequestBody Task task) {
		JSONObject json = new JSONObject();
		try {
			task = jpa.saveAndFlush(task);
			json.put("status", "SUCCESS");
			json.put("data", task.toString());
		} catch (Exception e) {
			e.printStackTrace();
			json.put("status", "FAILURE");
			json.put("data", e.getMessage());
		}
		return json.toString();
	}

	@PutMapping("/edit/task")
	@Override
	public void updatetask(@RequestBody Task task) {

	}

	@DeleteMapping("/delete/{Id}")
	@Override
	public String deletetask(@PathVariable Long Id) {
		JSONObject json = new JSONObject();
		try {
			boolean existflag = jpa.existsById(Id);
			if (existflag) {
				jpa.deleteById(Id);
				json.put("status", "SUCCESS");
			} else {
				json.put("status", "FAILURE");
				json.put("data", "Record does not exists!");
			}
		} catch (Exception e) {
			json.put("status", "FAILURE");
			json.put("data", e.getMessage());
		}
		return json.toString();
	}

	@GetMapping("/tasks/list")
	@Override
	public String gettasks() {
		// ObjectMapper mapper = new ObjectMapper();
		Map<String, String> map = new HashMap<>();
		List<JSONObject> list = new ArrayList<>();
		try {
			jpa.findAll().stream().forEach(element -> {
				JSONObject json = new JSONObject(element);
				list.add(json);
			});
			map.put("status", "SUCCESS");
			map.put("data", list.toString());
		} catch (Exception e) {
			map.put("status", "FAILURE");
			map.put("data", e.getMessage());
		}
		return map.toString();
	}

}
