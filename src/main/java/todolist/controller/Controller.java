package todolist.controller;

import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;

import todolist.model.Task;

public abstract interface Controller {
	String gettasks() throws JsonProcessingException;

	String savetask(Task task);

	void updatetask(Task task);

	String deletetask(Long Id);
}
